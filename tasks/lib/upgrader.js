var replace = require('applause'),
    upgraders = [
        'bootstrap2-3',
        'fontAwesome3-4'
    ];

module.exports = {
    run: function(input, type, grunt) {
        if (upgraders.indexOf(type) === -1) {
            grunt.log.warn(type + ' upgrader not valid.');

            return false;
        }

        var html = input;
        var rules = require('./upgraders/' + type + '.js');
        var len = rules.length;

        for (var i = 0; i < len; i++) {
            var rule = rules[i];
            try {
                var result = rule.run(html, replace, grunt);
                if (result) {
                    html = result;
                }
            } catch (e) {
                grunt.log.warn('Error processing the ruleset with title "' + rule.title + '"');
            }
        }

        // return parsed html or the input itself
        return html || input;
    }
};
