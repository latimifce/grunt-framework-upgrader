var sprintf = require("sprintf-js").sprintf;

module.exports = [
    {
        title: 'Revamped Grid System',
        description: 'Look for <code>spanX</code> non-form containers and replace with <code>col-lg-X</code> <code>col-md-X</code> <code>col-sm-X</code> (leaving mobile to collapse into a single column). Change <code>row-fluid</code> to <code>row</code> since they are now the same. Remove <code>container-fluid</code> since it is now a noop.',
        run: function (input, replace, grunt) {
            var options = {
                patterns: [
                    {
                        match: /offset(\d+)/g,
                        replacement: function () {
                            var match = arguments[1];
                            return 'col-md-offset-' + match;
                        }
                    },
                    {
                        match: /span(\d+)/g,
                        replacement: function () {
                            var match = arguments[1];
                            return sprintf('col-xs-12 col-sm-%(size)s col-md-%(size)s', { size: match });
                        }
                    },
                    {
                        match: 'row-fluid',
                        replacement: 'row'
                    },
                    {
                        match: 'container-fluid',
                        replacement: ''
                    },
                ],
                usePrefix: false
            };

            var applause = replace.create(options);
            return applause.replace(input).content;
        }
    },
    {
        title: "Changes to Button Color Classes",
        description: "Add <code>btn-default</code> to <code>btn</code> elements with no other color. Replace <code>btn-inverse</code> with <code>btn-default</code> since inverse has been removed from Bootstrap 3.",
        run: function (input, replace, grunt) {
            var options = {
                patterns: [
                    {
                        match: /"btn"/g,
                        replacement: '"btn btn-default"'
                    },
                    {
                        match: 'btn-inverse',
                        replacement: 'btn-default'
                    }
                ],
                usePrefix: false
            };

            var applause = replace.create(options);
            return applause.replace(input).content;
        }
    },
    {
        title: "Form Structural Changes",
        description: "<p>Forms have undergone some major changes. Here's some of what needs to happen:</p><ul><li><code>form-search</code> is gone, replace with <code>form-inline</code></li><li>Remove <code>input-block-level</code> as inputs are 100% width by default</li><li>Replace <code>help-inline</code> with <code>help-block</code> as inline is no longer supported</li><li>Change <code>.control-group</code> to <code>.form-group</code></li><li>Add column widths to horizontal form labels and <code>.controls</code></li><li>Remove <code>.controls</code> class</li><li>Add <code>form-control</code> class to inputs and selects.</li><li>Wrap checkboxes and radios in an extra <code>&lt;div&gt;</code></li><li>Replace <code>.radio.inline</code> and <code>.checkbox.inline</code> with <code>-inline</code> instead</li></ul>",
        run: function (input, replace, grunt) {
            var options = {
                patterns: [
                    {
                        match: 'form-search',
                        replacement: 'form-inline'
                    },
                    {
                        match: 'input-block-level',
                        replacement: ''
                    },
                    {
                        match: 'help-inline',
                        replacement: 'help-block'
                    },
                    {
                        match: 'control-group',
                        replacement: 'form-group'
                    },
                ],
                usePrefix: false
            };

            var applause = replace.create(options);
            return applause.replace(input).content;
        }
    },
    {
        title: "Dividers Removed from Breadcrumbs",
        description: "Bootstrap 3 uses CSS to add the dividers between breadcrumbs. Remove all <code>span.divider</code> elements inside breadcrumbs.",
        run: function(input, replace, grunt) {
            var options = {
                patterns: [
                    {
                        match: ' <span class="divider">/</span>',
                        replacement: ''
                    },
                ],
                usePrefix: false
            };

            var applause = replace.create(options);
            return applause.replace(input).content;
        }
    },
    {
        title: "Helper Class Specificity",
        description: "Prefix <code>muted</code> with <code>text-</code> and prefix <code>unstyled</code> and <code>inline</code> with <code>list-</code> (on <code>ul</code> and <code>ol</code> elements only).",
        run: function(input, replace, grunt) {
            var options = {
                patterns: [
                    {
                        match: 'class="muted',
                        replacement: 'class="text-muted'
                    },
                    {
                        match: 'class="unstyled',
                        replacement: 'class="list-unstyled'
                    },
                    {
                        match: 'class="inline',
                        replacement: 'class="list-inline'
                    },
                    {
                        match: 'class="inline',
                        replacement: 'class="list-inline'
                    },
                ],
                usePrefix: false
            };

            var applause = replace.create(options);
            return applause.replace(input).content;
        }
    },
    {
        title: "Hero Unit is now Jumbotron",
        description: "The component formerly known as a Hero Unit is now a Jumbotron, so swap <code>hero-unit</code> for <code>jumbotron</code>.",
        run: function(input, replace, grunt) {
            var options = {
                patterns: [
                    {
                        match: 'hero-unit',
                        replacement: 'jumbotron'
                    },
                ],
                usePrefix: false
            };

            var applause = replace.create(options);
            return applause.replace(input).content;
        }
    },
    {
        title: "Progress Bar Structural Changes",
        description: "The inner element class is now <code>progress-bar</code>, not <code>bar</code>. Additionally, the bar colors also have a <code>progress-</code> prefix.",
        run: function (input, replace, grunt) {
            var options = {
                patterns: [
                    {
                        match: 'class="bar',
                        replacement: 'class="progress-bar'
                    },
                ],
                usePrefix: false
            };

            var applause = replace.create(options);
            return applause.replace(input).content;
        }
    },
    {
        title: "Upgrade Responsive Classes",
        description: "Change responsive classes from <code>[visible|hidden]-[phone|tablet|desktop]</code> to <code>[visible|hidden]-[sm|md|lg]</code>",
        run: function(input, replace, grunt) {
            var options = {
                patterns: [
                    {
                        match: /(visible|hidden)-(phone|tablet|desktop)/g,
                        replacement: function () {
                            var prefix = arguments[1];
                            var device = arguments[2];

                            var sizes = {
                                'phone': 'xs',
                                'tablet': 'sm',
                                'desktop': 'md'
                            };
                            return prefix + '-' + sizes[device];
                        }
                    },
                ]
            };

            var applause = replace.create(options);
            return applause.replace(input).content;
        }
    },
    {
        title: "Upgrade Button, Pagination, and Well sizes.",
        description: "Change sizes from <code>[button|pagination|well]-[mini|small|large]</code> to <code>[button|pagination|well]-[xs|sm|lg]</code>",
        run: function(input, replace, grunt) {
            var options = {
                patterns: [
                    {
                        match: /(btn|pagination|well)-(mini|small|large)/g,
                        replacement: function () {
                            var prefix = arguments[1];
                            var device = arguments[2];

                            var sizes = {
                                'mini': 'xs',
                                'small': 'sm',
                                'large': 'lg'
                            };
                            return prefix + '-' + sizes[device];
                        }
                    },
                ]
            };

            var applause = replace.create(options);
            return applause.replace(input).content;
        }
    },
    {
        title: "Upgrade Alert Block Classes.",
        description: "<ul><li>Changes <code>.alert-block</code> to simply <code>.alert</code><br>Alerts without a modifier are defaulted to <code>.alert-warning</code>.</li><li><code>.alert-dismissable</code> added to all alerts that may be dismissed.</li></ul>",
        run: function (input, replace, grunt) {
            var options = {
                patterns: [
                    {
                        match: 'alert-block',
                        replacement: 'alert'
                    },
                ],
                usePrefix: false
            };

            var applause = replace.create(options);
            return applause.replace(input).content;
        }
    },
    {
        title: "Upgrade Label Classes.",
        description: "Changes <code>.label</code> to <code>.label.label-default</code>",
        run: function(input, replace, grunt) {
            var options = {
                patterns: [
                    {
                        match: 'class="label"',
                        replacement: 'class="label label-default"'
                    },
                ],
                usePrefix: false
            };

            var applause = replace.create(options);
            return applause.replace(input).content;
        }
    }
];
